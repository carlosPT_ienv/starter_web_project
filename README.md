## Steps to get Up and Runing after cloning this repo

- This project uses Redis as a Session Managent handler, so it is important before continuing, to install the redis server and the redis cli.

The conection to the redis server it is secured with a password and you need to configure it as follows:
Firt start the redis server and its redis cli before running the backend services, once running set the password in the redis cli instance with this command.

```bash
config set requirepass MYSECRETPASSWORD
```
After receiving back the OK message, go and set this same password in your environment variables file.

After setting the password all new conections to the redis server need to be authenticated with this password, so we are already set at this point, and when the backend services request conection to the redis server It would be already authenticated, because It is provisioned with the password.

Now you can run the backend services as usual with flask run and you got you covered in a developement status.

## Environment Variables Example file:
```python
SECRET_KEY='flask_secret_key'
WTF_CSRF_SECRET_KEY='flask_wtf_secret_key'
FLASK_CONFIG='development'

REDIS_HOST='redis_hostname'
REDIS_PORT='redis_port'
REDIS_PASSWORD='redis_pasword' 
```





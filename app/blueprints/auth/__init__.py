from flask import Blueprint

auth = Blueprint('auth', __name__)
gen_tk = Blueprint('auth/gen_csrf_tk', __name__)
chk_tk = Blueprint('auth/check_csrf_tk', __name__)

from .check_csrf_tk import views
from .gen_csrf_tk import views
from . import views
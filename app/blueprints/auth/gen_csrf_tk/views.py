from flask import session, request, current_app, jsonify

from flask_wtf.csrf import generate_csrf

from nomina_app.app.blueprints.auth.gen_csrf_tk import gen_tk

@gen_tk.after_request
def inject_csrf_token(response):
    '''
    Checking if a token it is already generated, if it is, it is not necessary to create another new token.
    -- A new token it is generated for new sessions only --
    '''
    # Generating the token based on Default config Values
    xsrf_token = None
    current_tk = None
    current_session = None
    new_session = None
    print("session BTK: ", session)

    print("GET_REQ: ", request.headers)

    # Redis Session Instance
    r = current_app.config["SESSION_REDIS"]

    cookie_name = current_app.config["SECURITY_CSRF_COOKIE"]["key"]

    if session.get("csrf_token") is None:
        
        xsrf_token = generate_csrf()
        print("New session started")
        print("NEW_SESS: ", session.get("csrf_token"))
        print("NEW_XSRF_TK: ", xsrf_token)
        tk_dict = {
            "csrf_tk_session": session.get("csrf_token"),
        }

        r.hmset("csrf", tk_dict)
        session["tk"] = xsrf_token
        
        response.headers['X-XSRF-Token'] = xsrf_token
        response.set_cookie(cookie_name, xsrf_token, samesite="Strict")

        return response
    else:
        print("Found the key")
        
        current_session = session.get('csrf_token')
        current_tk = session['tk']

        print("SESS_FOUND_KEY: ", session)

        response.headers['X-XSRF-Token'] = current_tk
        response.set_cookie(cookie_name, current_tk, samesite="Strict")
        
        return response

    return response

@gen_tk.route('/')
def index():
    return jsonify({"msg": "Tu token ha sido generado de manera exitosa"})

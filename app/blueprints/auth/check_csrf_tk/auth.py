from flask import request, jsonify, make_response, session, \
    current_app

from . import auth_ck

from ....decorators.csrf_tk_validation import validation_csrf_tk

@auth_ck.before_request
@validation_csrf_tk
def check_token():
    pass

@auth_ck.route('/test_auth', methods=['POST'])
def test_route_auth():
    print("Avanzando")
    return jsonify({"msg": "Well tested"})

from flask import request, jsonify, make_response, session, \
    current_app

from flask_wtf.csrf import validate_csrf, CSRFError, logger
import logging

from . import check_csrf_tk

from ....decorators.csrf_tk_validation import validation_csrf_tk

@check_csrf_tk.before_request
@validation_csrf_tk
def check_token():
    '''@validation_csrf_tk decorator check the validity and Integrity of the request header token generated in the backend'''
    pass

@check_csrf_tk.route('/index', methods=['POST'])
def prueba():
    print("Desde Prueba Index Endpoint")
    print("======= POST index: ", request.headers.get("X-Xsrf-Token"))
    request.data
    print(request.get_json())

    return make_response({"msg": "Tu mensaje ha sido recibido"})

@check_csrf_tk.route('/testing_csrf')
def test_csrf():
    return make_response({"msg": "Probando GET EndPoint exitosamente"})


@check_csrf_tk.route('/test_post', methods=['POST'])
def test_post_csrf():
    print("======= POST test_post: ", request.headers.get("X-Xsrf-Token"))
    print("===== Response test_post: ", request.get_json())
    return make_response({"msg": "Probando POST test_post EndPoint exitosamente"})

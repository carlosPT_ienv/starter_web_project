from flask import request, session, current_app

from flask_wtf.csrf import validate_csrf 

def validation_csrf_tk(f):
    def wrapper():
        res = f()
        # Redis Instances with all Session Information 
        r = current_app.config["SESSION_REDIS"]

        session['csrf_token'] = str(r.hget("csrf", "csrf_tk_session"), 'utf-8')
        print(session)

        for key in r.scan_iter("session:*"):
            '''Iterating over all sessions stored in the redis stored'''
            k_sess = r.get(key)
            k_str = k_sess.decode('cp1252')
            k_slice = k_str[43:].replace("”u.", "")

            if ("csrf" in k_str) and ("tk" in k_str):
                k_split = k_slice.split("”")
                tk_sess = k_split[2][2:]
                req_tk = request.headers.get("X-Xsrf-Token")
                if req_tk and (req_tk == tk_sess):
                    '''Verifying validity of the token by Comparing Request token coming in the Header against a valid token stored in the current Session'''
                    session['csrf_token']=k_split[0]
                    print("SESS: ", k_split)
                    print("CURR_SESS: ", session)
                    sk = current_app.config['WTF_CSRF_SECRET_KEY']
                    tk = current_app.config['WTF_CSRF_FIELD_NAME']

                    validate_csrf(data=req_tk, secret_key=sk, token_key=tk)
                        
        return res
    return wrapper
    
import os
from dotenv import load_dotenv
import redis
from flask_migrate import Migrate
from flask import render_template
from nomina_app.app import create_app, db

# from nomina_app.app import create_app, db

# Import the Gobal Config File
from nomina_app.config import config


dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

app = create_app(config[os.getenv('FLASK_CONFIG')] or config['default'])

# migrate = Migrate(app, db)

# @app.shell_context_processor
# def make_shell_context():
#     from app.models import user, post, comment, roles, reply

#     return dict(app=app, db=db, user=user.User, post=post.Post, comment=comment.Comment, follow=user.Follow, roles=roles, reply=reply.ReplyComment)

import unittest
@app.cli.command()
def test():
    tests = unittest.TestLoader().discover('app.tests')
    unittest.TextTestRunner(verbosity=2).run(tests)

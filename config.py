import os
import redis

from . import app

basedir = os.path.abspath(os.path.dirname(__file__))

class Config():
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'MSK-MySecretKey'
    WTF_CSRF_SECRET_KEY = os.environ.get('WTF_CSRF_SECRET_KEY') or 'MSK-SecureToken'

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Flask caching Config
    # CACHE_TYPE = "simple" # Flask-Caching related configs
    # CACHE_DEFAULT_TIMEOUT = 300
    # TOKEN_VALIDO = ''

    # Send Cookie with csrf-token. This is the default for Axios and Angular.
    SECURITY_CSRF_COOKIE = {"key": "XSRF-TOKEN"}
    SECURITY_CSRF_HEADER = "X-XSRF-TOKEN"
    WTF_CSRF_CHECK_DEFAULT = False
    WTF_CSRF_TIME_LIMIT = None

    # Redirect to Angular by default
    SECURITY_REDIRECT_BEHAVIOR = "spa"
    SECURITY_REDIRECT_HOST = "localhost:4200"

    # Redis Config for Flask-Session use
    SESSION_TYPE = 'redis'
    SESSION_USE_SIGNER = True
    

    REDIS_HOST = os.environ.get('REDIS_HOST')
    REDIS_PORT = os.environ.get('REDIS_PORT')
    REDIS_PASS = os.environ.get('REDIS_PASSWORD')

    r = redis.Redis(
        host=REDIS_HOST,
        port=REDIS_PORT,
        password=REDIS_PASS        
    )

    # r.config_set("requirepass", "6789")
    
    SESSION_REDIS = r

    # Mail config envs 
#    FLASKY_ADMIN= os.environ.get('FLASKY_ADMIN')

    @staticmethod
    def init_app(app):
        pass
class TestingConf(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'TEST_DATABASE_URL') or 'sqlite:///'

class DevelopmentConf(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'DEV_DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')

class ProductionConf(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'data.sqlite')


config = {
    'development': DevelopmentConf,
    'production': ProductionConf,
    'Testing': TestingConf,

    'default': DevelopmentConf
}